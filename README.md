This repository contains a supplemental report for the paper Rank Aggregation Algorithms for Fair Consensus by Caitlin Kuhlman and Elke Rundensteiner 
which was submitted to VLDB 2020 on March 1, 2020.

All work is subject to copyright by the authors. The report will be published upon acceptance and subsequent publication of the paper.

This repo is made available to the reviewers. Thank you!